### docker 安装ElasticSearch

```html
	docker pull elasticsearch:6.8.1

	docker run --name elasticsearch6.8.1 -d -e ES_JAVA_OPTS="-Xms512m -Xmx512m" --net host -e "discovery.type=single-node" -p 9200:9200 -p 9300:9300 elasticsearch:6.8.1

	上传ik分词器并解压：unzip elasticsearch-analysis-ik-6.8.1.zip -d ik-analyzer

	docker cp ./ik-analyzer elasticsearch6.8.1:/usr/share/elasticsearch/plugins

	docker restart elasticsearch6.8.1

安装kibana：
	docker pull kibana:6.8.1
	
	docker run --name kibana6.8.1 -e ELASTICSEARCH_URL=http://172.16.116.100:9200 -p 5601:5601 -d kibana:6.8.1
	需要等待一会儿访问：http://172.16.116.100:5601
```

### 商品索引映射

```xml
PUT product
{
  "mappings": {
    "properties": {
      "skuId": {
        "type": "long"
      },
      "spuId": {
        "type": "keyword"
      },
      "skuTitle": {
        "type": "text",
        "analyzer": "ik_smart"
      },
      "skuPrice": {
        "type": "keyword"
      },
      "skuImg": {
        "type": "keyword"
      },
      "saleCount": {
        "type": "long"
      },
      "hasStock": {
        "type": "boolean"
      },
      "hotScore": {
        "type": "long"
      },
      "brandId": {
        "type": "long"
      },
      "catalogId": {
        "type": "long"
      },
      "brandName": {
        "type": "keyword"
      },
      "brandImg": {
        "type": "keyword"
      },
      "catalogName": {
        "type": "keyword"
      },
      "attrs": {
        "type": "nested",
        "properties": {
          "attrId": {
            "type": "long"
          },
          "attrName": {
            "type": "keyword"
          },
          "attrValue": {
            "type": "keyword"
          }
        }
      }
    }
  }
}
```

### 检索与聚合映射

````xml
#检索DSL
GET product/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "skuTitle": "华为"
          }
        }
      ],
      "filter": [
        {
          "term": {
            "catalogId": "225"
          }
        },
        {
          "terms": {
            "brandId": [
              "1",
              "2"
            ]
          }
        },
        {
          "nested": {
            "path": "attrs",
            "query": {
              "bool": {
                "must": [
                  {
                    "term": {
                      "attrs.attrId": {
                        "value": "15"
                      }
                    }
                  },
                  {
                    "terms": {
                      "attrs.attrValue": [
                        "海思",
                        "骁龙"
                      ]
                    }
                  }
                ]
              }
            }
          }
        },
        {
          "term":{
            "hasStock":{
              "value":"true"
            }
          }
        },
        {
          "range":{
            "skuPrice":{
              "gte":100,
              "lte":6000
            }
          }
        }
      ]
    }
  },
  "sort": [
    {
      "skuPrice": {
        "order": "desc"
      }
    }
  ]  ,
  "from": 0, 
  "size": 10,
  "highlight": {
    "fields": {"skuTitle": {}}, 
    "pre_tags": "<em style='color:red'>",
    "post_tags": "</em>"
  }
}

#聚合DSL
GET product/_search
{
  "query": {
    "match_all": {}
  },
  "aggs": {
    "brand_agg": {
      "terms": {
        "field": "brandId",
        "size": 100
      },
      "aggs": {
        "brand_name_agg": {
          "terms": {
            "field": "brandName",
            "size": 100
          }
        },
        "brand_Img_agg": {
          "terms": {
            "field": "brandImg",
            "size": 100
          }
        }
      }
    },
    "catalog_agg": {
      "terms": {
        "field": "catalogId",
        "size": 100
      }
      , "aggs": {
        "catalog_name_agg": {
          "terms": {
            "field": "catalogName",
            "size": 100
          }
        }
      }
    },
    "attr_agg": {
      "nested": {
        "path": "attrs"
      },
      "aggs": {
        "attr_id_agg": {
          "terms": {
            "field": "attrs.attrId",
            "size": 100
          },
          "aggs": {
            "attr_name_age": {
              "terms": {
                "field": "attrs.attrName",
                "size": 100
              }
            },
            "attr_value_age": {
              "terms": {
                "field": "attrs.attrValue",
                "size": 100
              }
            }
          }
        }
      }
    }
  }
}
````

